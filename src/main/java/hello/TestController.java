package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class TestController {
	
	private static final String template = "Testing, %s!";
    private final AtomicLong counter = new AtomicLong();
    String test;

    @RequestMapping("/test")
    public Greeting greeting(@RequestParam(value="name", defaultValue="Test123") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name),test);
            }

}
